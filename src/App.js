import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import axios from "axios";

// 1. Tworzę komponent Input z dwoma atrybutami ({value, onChange})
// 2. Tworzę klasę Form extends Component , która wytowrzy stan komponentu

const InputText = ({ value, onChange }) => {
  return (
    <div>
      <input
        placeholder="Wpisz tekst"
        type="email"
        value={value}
        onChange={event => {
          onChange(event.target.value);
        }}
      />
    </div>
  );
};

const Btn = ({ onClick }) => {
  return (
    <div>
      <button onClick={onClick}>Wciśnij mnie</button>
    </div>
  );
};

// Tworzę komponent stanowy. Klasa Form rozszerza klasę Component

class Form extends Component {
  constructor(props) {
    super(props);

    // klucz this.state wytwarza nam stan komponentu. Przyjmuje "klucz":"wartość"
    this.state = {
      inputValue: ""
    };

    this.onChange = this.onChange.bind(this);
  }

  onChange(value) {
    console.log(value);

    // aktualizacja stanu komponentu
    this.setState({
      inputValue: value
    });
  }

  render() {
    const { inputValue } = this.state;

    return (
      <form>
        <h1>Formularz</h1>

        <InputText value={inputValue} onChange={this.onChange} />
        <Btn
          onClick={event => {
            event.preventDefault();
            console.log("kliknięto przycisk");
          }}
        />

        {/* JSON.stringify słuzy do sparsowania obiektów na string. Wyświetli nam stan komponentu ("klucz":"wartość") */}
        {/* {JSON.stringify(this.state)} */}
      </form>
    );
  }
}

const TodoItem = ({ title }) => {
  return <li className="">{title}</li>;
};

const LoadingPage = () => {
  return (
    <div>
      <h1>Loading</h1>
    </div>
  );
};

class TodoList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todos: [],
      imBusy: true
    };
  }

  componentDidMount() {
    axios.get("http://195.181.210.249:3000/todo/").then(response => {
      console.log(response);
      if (response.status === 200) {
        this.setState({
          todos: response.data,
          imBusy: false
        });
      }
    });
  }

  render() {
    const { todos, imBusy } = this.state;

    if (imBusy === true) {
      return <LoadingPage />;
    } else {
      const todo = this.state.todos.map(todo => (
        <TodoItem title={todo.title} key={todo.id} />
      ));
      return <ul>{todo}</ul>;
    }
  }
}

const App = () => {
  return (
    <div className="App">
      <div className="container">
        <Form />
        <TodoList />
      </div>
    </div>
  );
};

export default App;
