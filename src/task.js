import React, { Component } from "react";
// 1. z podanego htmla wytworzyć aktywnie działające komponenty stanowe
const InputHtml = ({ value, onChange }) => {
  return (
    <div>
      <input
        placeholder="Wpisz tekst"
        type="email"
        value={value}
        onChange={ev => {
          onChange(ev.target.value);
        }}
      />
    </div>
  );
};

class InputExtended extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ""
    };
    // w tym komponencie wartosc inputa ma być przechowywana w stanie pod kluczem "inputValue"
    // ten komponent ma mieć metodę onInputChange która loguje zmiany wartości do konsoli

    this.onChange = this.onChange.bind(this);
  }

  onChange(value) {
    console.log(value);
  }
  render() {
    const email = this.state;

    return (
      <form>
        <h1>Formularz</h1>
        {JSON.stringify(this.state)}
      </form>
    );
  }
}

export { InputHtml };
export { InputExtended };
